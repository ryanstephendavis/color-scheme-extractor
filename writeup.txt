PREPARATION LIST:
------------------
4 quadrants:
    slides
    2 terminals with virtual environments activated
        -> prepopulate commands
            -> interactive mode with firefox pictures up
        -> time with --euclidean set and not set
    2 firefox windows to display html & picture 
        -> start with cwleonis_swas_big.jpg & cwleonis_sorted_rgb.html
    Pycharm

MAKE SURE SLIDES HAVE THE FOLLOWING REMINDERS:
----------------------------------------------
Bro mode to see live Pycharm debug

Command line Interactive mode to see idea of thresholds

Timing out run of euclidean vs. manhattan distance
  -> show big_pic_6* html files with 6i3wgxtvgaq31.png

If time permits, show explain binary search for num of desired bins
    binary search explanation picture source:
    http://unfolding-programming.net/essays/2019-10-30-on-binary-search
       -> into slides 

TODOs
update website -> if this doesn't happen, go to 

matplotlib scatter plot of cwleonis RGB values

time each part to look for optimizations

re-paint the picture with only binned colors, dither first?
