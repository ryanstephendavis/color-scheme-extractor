#!/usr/bin/env python
import sys
import argparse
from PIL import Image
from math import sqrt


preamble = \
'''
<!DOCTYPE html>
<html class="" lang="en-US">
    <head>
        <title>TITLE!</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
'''
ending = \
'''
            </div>
        </div>
    </body>

</html>
'''
line_begin = '<div class="col-sm" style="text-align:center; background-color:rgb'
line_end = '</div>'


def euclidean_distance(p1, p2):
    return sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2 + (p1[2] - p2[2])**2)


def manhattan_distance(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1]) + abs(p1[2] - p2[2])


def get_rgb_counts_for_every_tuple_in_image(image_path):
    rgb_counts = {}
    with Image.open(image_path) as image:
        for x in range(0, image._size[0]):
            for y in range(0, image._size[1]):
                p = image.getpixel((x, y))
                if rgb_counts.get(p) is None:
                    rgb_counts[p] = 1
                else:
                    rgb_counts[p] += 1
    return rgb_counts


def get_sorted_rgb_counts(rgb_counts):
    """
    return_value: sorted_rgb_cnt = [ ((R1,G1,B2), count1), ((R2,G2,B2), count2) ... ]
    """
    return sorted(rgb_counts.items(), key=lambda x: x[1], reverse=True)


def output_html(color_bins, output_file_path):
    if not output_file_path.endswith('.html'):
        output_file_path = f'{output_file_path}.html'
    # Output to html
    with open(output_file_path, 'w') as f:
        f.write(preamble)
        for i, rgb in enumerate(color_bins):
            f.write(line_begin + f'{rgb}' + ';">' + f'{rgb}' + line_end)
        f.write(ending)


def enter_interactive_mode(rgb_counts_sorted, output_file_path):
    while True:
        threshold = input('Enter a threshold (distance for binning colors) or q to quit:')
        if threshold == 'q':
            print("Good conversation, eh... hope you don't not enjoy your colors bud")
            break
        try:
            threshold = int(threshold)
        except:
            print('Not a valid input, must be int')
            continue
        
        color_bins = bin_my_colors(rgb_counts_sorted, threshold)
        output_html(color_bins, output_file_path)
   

def bin_my_colors(rgb_counts_sorted, threshold, euclidean=False):
    distance_func = euclidean_distance if euclidean else manhattan_distance
    # The most pixels have this RGB color so add that as first bin 
    color_bins = [rgb_counts_sorted[0][0]]  

    for rgb_and_count in rgb_counts_sorted:
        rgb = rgb_and_count[0]
        found_bin = False
        for bin_color in color_bins:
            if distance_func(rgb, bin_color) <= threshold:
                found_bin = True 
                break  # No need to check other bins
        if not found_bin:  # Make a new color bin
            color_bins.append(rgb)

    return color_bins


def find_color_bins(rgb_counts_sorted, num_bins_desired, euclidean=False):
    # max_distance = 765  # 255 + 255 + 255 using manhattan distance
    up_threshold = 765
    low_threshold = 0  # minimum distance
    mid_threshold = (up_threshold - low_threshold) // 2 + low_threshold
    i = 0
    bins_to_threshold_map = {}
    while True:
        i += 1
        print(f'Iteration {i}')
        print(f'up_threshold={up_threshold}')
        print(f'mid_threshold={mid_threshold}')
        print(f'low_threshold={low_threshold}')
        color_bins = bin_my_colors(rgb_counts_sorted, mid_threshold, euclidean)

        # Store away num of bins to threshold in case of no solution
        num_bins_found = len(color_bins)
        bins_to_threshold_map[num_bins_found] = mid_threshold
        print(f'num_bins_found={num_bins_found}')

        # Adjust search range accordingly
        if num_bins_found == num_bins_desired:
            return color_bins
        elif num_bins_found > num_bins_desired:
            # need higher threshold
            low_threshold = mid_threshold
        else:  # num_bins_found < num_bins_desired:
            # need lower threshold
            up_threshold = mid_threshold

        mid_threshold = (up_threshold - low_threshold) // 2 + low_threshold

        # No solution
        if (up_threshold == mid_threshold or
            low_threshold == mid_threshold):
            print(f"Can't solve for {num_bins_found}, returning previous bins")
            print(f'Found possible solutions {bins_to_threshold_map}')
            return color_bins


def main(input_path, output_path,
         num_bins_desired=10, interactive_mode=False, bro_mode=False, euclidean=False):
    rgb_counts = get_rgb_counts_for_every_tuple_in_image(input_path)
    rgb_counts_sorted = get_sorted_rgb_counts(rgb_counts)

    if bro_mode:  # Verbose mode for presentation only
        # Output all color for presentation purposes
        color_bins = [color_and_count[0] for color_and_count in rgb_counts_sorted]
        output_html(color_bins, f'{output_path}_sorted_rgb')

        # Take the most popular colors bro!
        color_bins = rgb_counts_sorted[:num_bins_desired]
        color_bins = [color_and_count[0] for color_and_count in color_bins]
        output_html(color_bins, f'{output_path}_most_popular')

    if interactive_mode:
        enter_interactive_mode(rgb_counts_sorted, output_path)
    else:  # Search for the desired number of bins
        color_bins = find_color_bins(rgb_counts_sorted, num_bins_desired, euclidean)
        if color_bins:
            output_html(color_bins, output_path)
        else:
            print('Sumthin went wrong findin bins yo...no html fo you')

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--image', '-i',
                        required=True,
                        type=str,
                        help='Image path from which colors shall be extracted')
    parser.add_argument('--output_file', '-o',
                        required=True,
                        type=str,
                        help='Output file name')
    parser.add_argument('--num-bins', '-b',
                        type=int,
                        help='Number of colors to extract for final scheme,'
                             'ignored if interactive')
    parser.add_argument('--interactive', '-int',
                        action='store_true', 
                        help='Prompts user for threshold and bins colors interactively')
    parser.add_argument('--bromode',
                        action='store_true',
                        help='Use "most popular colors douchey bro mode '
                             '(this is bad, for presentation purposes only')
    parser.add_argument('--euclidean', '-e',
                        action='store_true',
                        help='Use euclidean instead of manhattan distance for calculations')
                        
    args = parser.parse_args()
     
    sys.exit(
        main(
            args.image,
            args.output_file,
            num_bins_desired=args.num_bins,        
            interactive_mode=args.interactive,
            bro_mode=args.bromode,
            euclidean=args.euclidean
        )
    )
